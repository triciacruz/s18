//console.log("hello");

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

		//Addition
		function addNum(num1, num2){
		let add = num1 + num2;
		console.log("Displayed sum of " + num1 + " and " + num2);
		console.log(add);
		}
		addNum(5, 15);

		//Subtraction
		function subNum(num1, num2){
		let sub = num1 - num2;
		console.log("Displayed difference of " + num1 + " and " + num2);
		console.log(sub);
		}
		subNum(20, 5);

/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
		//Product		
		function returnProdNum(num1, num2){
		let prodSol = num1 * num2;
		return prodSol;
		}
		console.log("The product of 50 and 10:");
		let product = returnProdNum(50,10);
		console.log(product);

		// Quotient
		function returnQuoNum(num1, num2){
		let quotient = num1 / num2;
		return quotient;
		}
		console.log("The quotient of 50 and 10:");
		let quoAns = returnQuoNum(50,10);
		console.log(quoAns);
				

/*		
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

		// Area of a circle
		function returnCircleArea(rad){
		let area = 3.1416 * (rad * rad);
		return area;
		}
		console.log("The area of circle with 15 radius:");
		let areaAns = returnCircleArea(15);
		console.log(areaAns);

/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/
		
		// Average
		function returnAverage(grade1, grade2, grade3, grade4){
		let average= (grade1 + grade2 + grade3 + grade4)/4;
		return average;
		}
		console.log("The average of 20, 40, 60 and 80:");
		let averageAns = returnAverage(20, 40, 60, 80);
		console.log(averageAns);

/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
	/*// Score
		function returnScore(score){
		return score;
		return isPassing = score >= 38;
		console.log(isPassing);
		}
		console.log("Is 38/50 a passing score?");
		let scoreAns = returnScore(true);
		console.log(scoreAns);
*/

		function returnScore(score){
			let percentage = (score/50)*100;

			let isPassing = percentage >= 75;
			return isPassing;
		}
		console.log("Is 38/50 a passing score?");
		let scoreAns = returnScore(38);
		console.log(scoreAns);
	